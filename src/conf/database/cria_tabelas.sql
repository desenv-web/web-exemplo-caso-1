--------------------------------------------------------------------------------
-- Cria as tabelas para cadastro das inscrições
--------------------------------------------------------------------------------
create table categoria (
    codigo int not null,
    descricao text not null,
    valor1_inscricao float not null,
    valor2_inscricao float not null,
    valor1_minicurso float not null,
    valor2_minicurso float not null,
    valor1_visita float not null,
    valor2_visita float not null,
    primary key (codigo)
);
alter table categoria owner to desenvweb;

insert into categoria values (1, 'Profissionais / Professores / Pesquisadores', 300, 350, 80, 90, 0, 0);
insert into categoria values (2, 'Estudantes', 200, 250, 50, 60, 0, 0);
--------------------------------------------------------------------------------
create table situacao (
    codigo int not null,
    descricao varchar(50) not null,
    icone text, -- URL do icone para a situacao
    primary key (codigo)
);
alter table situacao owner to desenvweb;

insert into situacao values (1, 'Pendente',   '/imagens/bola_amarela_10x10.png');
insert into situacao values (2, 'Confirmada', '/imagens/bola_verde_10x10.png');
insert into situacao values (3, 'Cancelada',  '/imagens/bola_verm_10x10.png');
--------------------------------------------------------------------------------
create table minicurso (
    codigo int not null,
    descricao text not null,
    ministrante text not null,
    vagas int not null,
    primary key (codigo)
);
alter table minicurso owner to desenvweb;

insert into minicurso values (1, 'Revisão Sistemática da Literatura: Como Obter Resultados Úteis', 'Joselaine Valaski', 130);
insert into minicurso values (2, 'Advancements in Intelligent Support for Collaborative Learning', 'Seiji Isotani', 130);
insert into minicurso values (3, 'Ontologias para Padrões de Especificação de Conteúdos Instrucionais', 'André Menolli', 130);
--------------------------------------------------------------------------------
create table visita (
    codigo int not null,
    descricao text not null,
    vagas int not null,
    primary key (codigo)
);
alter table visita owner to desenvweb;
--------------------------------------------------------------------------------
create table inscricao (
    numero serial not null,
    cpf numeric(11) not null,
    identidade varchar(15) not null,
    instituicao varchar(100) not null,
    categoria int not null,
    nome varchar(50) not null,
    fone varchar(20) not null,
    email varchar(100) not null,
    endereco varchar(100) not null,
    complemento varchar(20),
    bairro varchar(50),
    cidade varchar(50) not null,
    estado varchar(2) not null,
    cep int not null,
    camiseta varchar(2),
    participacao boolean not null,
    data_hora timestamp not null default current_timestamp,
    situacao int not null default 1, -- 1=pendente, 2=confirmada, 3=cancelada
    primary key (numero),
    unique (cpf),
    foreign key (situacao) references situacao (codigo),
    foreign key (categoria) references categoria (codigo)
);
alter table inscricao owner to desenvweb;
--------------------------------------------------------------------------------
create table inscricao_minicurso (
    numero_inscricao int not null,
    minicurso int not null,
    data_hora timestamp not null default current_timestamp,
    situacao int not null default 1, -- 1=pendente, 2=confirmada, 3=cancelada
    primary key (numero_inscricao, minicurso),
    foreign key (situacao) references situacao (codigo),
    foreign key (numero_inscricao) references inscricao (numero),
    foreign key (minicurso) references minicurso (codigo)
);
alter table inscricao_minicurso owner to desenvweb;
--------------------------------------------------------------------------------
create table inscricao_visita (
    numero_inscricao int not null,
    visita int not null,
    data_hora timestamp not null default current_timestamp,
    situacao int not null default 1, -- 1=pendente, 2=confirmada, 3=cancelada
    primary key (numero_inscricao, visita),
    foreign key (situacao) references situacao (codigo),
    foreign key (numero_inscricao) references inscricao (numero),
    foreign key (visita) references visita (codigo)
);
alter table inscricao_visita owner to desenvweb;
--------------------------------------------------------------------------------
create table inscricao_pagamento (
    numero_documento serial not null,
    numero_inscricao int not null,
    data_hora_processamento timestamp,
    data_vencimento date,
    valor_documento float not null,
    data_pagamento date,
    valor_pago float,
    valor_tarifa float,
    banco_pagamento int,
    data_retorno date,
    data_hora_confirmacao timestamp,
    primary key (numero_documento),
    foreign key (numero_inscricao) references inscricao (numero)
);
alter table inscricao_pagamento owner to desenvweb;
--------------------------------------------------------------------------------
create table situacao_artigo (
    codigo int not null,
    descricao varchar(50) not null,
    icone text, -- URL do icone para a situacao
    primary key (codigo)
);
alter table situacao_artigo owner to desenvweb;

insert into situacao_artigo values (1, 'Submetido',    '/imagens/bola_amarela_10x10.png');
insert into situacao_artigo values (2, 'Aceito',       '/imagens/bola_verde_10x10.png');
insert into situacao_artigo values (3, 'Rejeitado',    '/imagens/bola_verm_10x10.png');
--------------------------------------------------------------------------------
create table artigo (
    autor int not null,
    seq int not null,
    data_submissao timestamp not null default current_timestamp,
    titulo varchar(200) not null,
    resumo text,
    arquivo text not null,
    situacao int not null default 1,
    primary key (autor, seq),
    foreign key (autor) references inscricao (numero),
    foreign key (situacao) references situacao_artigo (codigo)
);
alter table artigo owner to desenvweb;
--------------------------------------------------------------------------------
create table cronograma (
    codigo int not null,
    data1 date not null,
    data2 date not null,
    data3 date not null,
    primary key (codigo)
);
alter table cronograma owner to desenvweb;
----------------------------------------------------------------------------------
insert into cronograma values (1, '2016-09-01', '2016-10-03', '2016-10-06');
----------------------------------------------------------------------------------
