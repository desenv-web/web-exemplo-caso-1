/*
 * === CONVERSORES ===
 * Este código fonte pode ser livremente copiado, modificado e distribuído,
 * desde que esta mensagem seja preservada.
 * Todo o esforço foi feito para escrever um código correto e eficiente, entretanto o
 * autor não assume qualquer responsabilidade, direta ou indireta sobre a aplicabilidade
 * e/ou adequação a um fim específico e não garante que esteja livre de erros.
 * Copyright (C) 2011 <a href="http://www.utfpr.edu.br">Universidade Tecnológica Federal do Paraná</a>
 */
package evento.faces.convert;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * Converte um string contendo telefone em um formato no padrão brasileiro.
 * O formato usado é (NN) NNNN-NNNN.
 * @author Wilson
 */
@FacesConverter("PhoneConverter")
public class PhoneConverter implements Converter {

    private static final String noPhonePattern = "[^\\d\\s\\-()]+";

    /**
     * Verifica se um string pode ser um número de telefone.
     * Um string é considerado candidato a número de telefone se contém somente
     * caracteres numéricos (0-9), espaços, hífens (-) e parênteses.
     * Esta função realiza apenas uma verificação básica e não verifica se o
     * número é sintaticamente correto.
     * @param s O string a ser testado
     * @return {@code true} se o string puder ser interpretado como um número de telefone
     */
    private boolean isPhoneCandidate(String s) {
       return !s.matches(noPhonePattern);
    }

    /**
     * Remove todos os caracteres não numéricos do telefone.
     * @param phone O telefone original
     * @return O telefone somente com caracteres numéricos
     */
    private StringBuffer removePunctuation(StringBuffer phone) {
        for (int i = phone.length()-1; i >= 0; i--) {
            char c = phone.charAt(i);
            if (c < '0' || c > '9')
                phone.deleteCharAt(i);
        }
        return phone;
    }

    /**
     * Formato no número de telefone, sem pontuação, no formato (NN) NNNN-NNNN.
     * Dependendo o comprimento do string original, pode não conter todas as
     * partes do telefone.
     * @param phone O número original, sem pontuação
     * @return O número de telefone formatado
     */
    private String formatPhone(StringBuffer phone) {
        int len = phone.length();
        if (len > 4) phone.insert(len-4, '-');
        if (len > 8) {
            phone.insert(len-8, ") ");
            phone.insert(0, '(');
        }
        return phone.toString();
    }

    /**
     * Formata o string original no formato (NN) NNNN-NNNN
     * Dependendo do string original, pode não conter todas as partes ou retornar
     * o próprio string original.
     * @param s O string original
     * @return O string original formatado
     */
    private String formatPhone(String s) {
        if (s == null) return "";
        String phone = s.trim();
        if (phone.isEmpty() || !isPhoneCandidate(phone)) return s;
        return formatPhone(removePunctuation(new StringBuffer(s)));
    }

    /**
     * Retorna apenas o string recebido uma vez que este conversor
     * atua como formatador e o valor recebido deve ter sido formatado previamente.
     * @param fc O contexto
     * @param uic O componente
     * @param phone O string original
     * @return O string formatado
     */
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String phone) {
        return phone;
    }

    /**
     * Tenta converter o string original no formato (NN) NNNN-NNNN.
     * Caso o string original contenha caracteres inválidos, retorna o string
     * original.
     * @param fc O contexto
     * @param uic O componente
     * @param o O string já formatado
     * @return O objeto convertido para string
     */
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null && !(o instanceof String))
            throw new ConverterException("O telefone deve ser um objeto da classe String. "
                    + "A classe encontrada é '" + o.getClass().getName() + "'.");
        return formatPhone((String)o);
    }

}
