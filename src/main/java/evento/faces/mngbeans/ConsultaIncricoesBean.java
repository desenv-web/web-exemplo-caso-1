package evento.faces.mngbeans;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.controller.SituacaoJpaController;
import evento.persistence.entity.Inscricao;
import evento.persistence.entity.Situacao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class ConsultaIncricoesBean extends  PageBean {
    private static final long serialVersionUID = 1L;
    private DataModel<Inscricao> inscricoesDataModel;

    public ConsultaIncricoesBean() {
    }

    public Date getMinDataVencto() {
        GregorianCalendar vencto = new GregorianCalendar();
        vencto.add(Calendar.DAY_OF_MONTH, 2);
        return vencto.getTime();
    }
    
    public void venctoChangeListener(ValueChangeEvent event) {
        return;
    }
    
    public List<Inscricao> getInscricoes() {
        InscricaoJpaController ctl = new InscricaoJpaController();
        return ctl.findAllOrderByNumero();
    }
    
    public DataModel<Inscricao> getInscricoesDataModel() {
        if (inscricoesDataModel == null)
            inscricoesDataModel = new ListDataModel<>(getInscricoes());
        return inscricoesDataModel;
    }
    
    public List<SelectItem> getSituacaoFilterOptions() {
        List<SelectItem> items;
        try {
            SituacaoJpaController ctl = new SituacaoJpaController();
            items = new ArrayList<>();
            items.add(new SelectItem("", "Todas"));
            for (Situacao s : ctl.findAll()) {
                items.add(new SelectItem(s.getCodigo(), s.getDescricao()));
            }
        } catch (Throwable t) {
            items = Collections.emptyList();
            error("Não foi possível obter a lista de situações");
            log("getSituacaoFiterOptions", t);
                    
        }
        return items;
    }

    public void excluiInscricaoAction() {
        try {
            if (inscricoesDataModel != null && inscricoesDataModel.isRowAvailable()) {
                InscricaoJpaController ctl = new InscricaoJpaController();
                Inscricao inscricao = inscricoesDataModel.getRowData();
                ctl.remove(inscricao);
                info(String.format("Inscrição número %d excluída", inscricao.getNumero()));
                inscricoesDataModel = null;
            }
            
        } catch (Throwable t) {
            error("Não foi possível excluir inscrição");
            log("excluiInscricaoAction", t);
                    
        }
    }
}
