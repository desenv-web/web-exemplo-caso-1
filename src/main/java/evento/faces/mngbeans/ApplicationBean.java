/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.faces.mngbeans;

import javax.faces.application.Application;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Wilson
 */
@ManagedBean(eager=true)
@ApplicationScoped
public class ApplicationBean extends utfpr.faces.support.ApplicationBean {
    private static final long serialVersionUID = 1L;

    public ApplicationBean() {
    }
}
