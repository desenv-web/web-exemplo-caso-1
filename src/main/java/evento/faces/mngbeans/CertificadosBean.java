/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.faces.mngbeans;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Inscricao;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@RequestScoped
public class CertificadosBean extends PageBean {

    private Inscricao inscricao;
    private Long cpf;
    private String fone;
    private String email;

    public CertificadosBean() {

    }

    public Inscricao getInscricao() {
        return inscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private File[] getFiles(){
        File path = new File(getExternalContext().getRealPath("/certificados"));
        return path.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                String mask = String.format("%03d.pdf", inscricao.getNumero());
                String fname = name.toLowerCase();
                return fname.endsWith(mask) && fname.matches("^[cpm].+$");
            }
        });
    }

    public List<String[]> getLinks() {
        ArrayList<String[]> links = new ArrayList<>();
        for (File f: getFiles()) {
            String texto = "", link = "certificados/" + f.getName();
            switch (f.getName().toLowerCase().charAt(0)) {
                case 'c':
                    texto = "Certificado de participação no evento";
                    break;
                case 'p':
                    texto = "Certificado de apresentação de trabalho";
                    break;
                case 'm':
                    texto = "Certificado do Minicurso " + f.getName().charAt(1);
                    break;
            }
            links.add(new String[]{link, texto});
        }
        return links;
    }

    public void certificadosAction() {
        InscricaoJpaController ctl = new InscricaoJpaController();
        try {
            inscricao = ctl.findByCPF(cpf);
            if (inscricao == null || !inscricao.getFone().equals(fone)
                    || !inscricao.getEmail().equals(email)) {
                error("Inscrição não localizada. Por favor, certifique-se de "
                        + "que os dados informados estão corretos.");
            } else {

            }
        } catch (Exception e) {
            log("", e);
            error("Erro na consulta: " + e.getLocalizedMessage());
        } finally {
            ctl.close();
        }
    }
}
