package evento.faces.mngbeans;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Inscricao;
import evento.persistence.entity.InscricaoMinicurso;
import evento.persistence.entity.InscricaoPagamento;
import evento.persistence.entity.InscricaoVisita;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import utfpr.faces.support.RequestBean;
import utfpr.util.DVModulo10;
import utfpr.util.DVModulo11;

@ManagedBean
@RequestScoped
public class BoletoBean extends RequestBean {

    private Integer numeroInscricao;
    private Integer numeroPagamento;
    private InscricaoJpaController inscricaoJpaController;
    private boolean avulso;
    private Date dataDocumento = new Date();
    private Date dataProcessamento = dataDocumento;
    private final DVModulo11 dvMod11 = new DVModulo11();
    private final DVModulo10 dvMod10 = new DVModulo10();
    private static final ResourceBundle paramBoleto = ResourceBundle.getBundle("cema.boleto.campos");
    private static final ResourceBundle config = ResourceBundle.getBundle("cema.config.interface");
    private static final Calendar dataBaseVencimento = new GregorianCalendar(1997, Calendar.OCTOBER, 7);
    private List<String> instrucoes;

    public BoletoBean() {
    }

    public BoletoBean(Integer numeroInscricao, Integer numeroPagamento) {
        this.numeroInscricao = numeroInscricao;
        this.numeroPagamento = numeroPagamento;
    }

    public Integer getNumeroInscricao() {
        return numeroInscricao;
    }

    public void setNumeroInscricao(Integer numeroInscricao) {
        this.numeroInscricao = numeroInscricao;
    }

    public Integer getNumeroPagamento() {
        return numeroPagamento;
    }

    public void setNumeroPagamento(Integer numeroPagamento) {
        this.numeroPagamento = numeroPagamento;
    }

    private Inscricao getInscricao(boolean lazy) {
        inscricaoJpaController = new InscricaoJpaController(lazy);
        return numeroInscricao == null ? null : inscricaoJpaController.findByNumero(numeroInscricao);
    }

    public Inscricao getInscricao() {
        return getInscricao(false);
    }

    public Inscricao getInscricaoLazy() {
        return getInscricao(true);
    }

    private void closeInscricaoJpaController() {
        inscricaoJpaController.close();
    }

    public int getPessoaAvulso() {
        return (int) ((System.currentTimeMillis() / 100) % 100000000);
    }

    public boolean isAvulso() {
        return avulso;
    }

    public void setAvulso(boolean avulso) {
        this.avulso = avulso;
    }

    public String getNomeSacado() {
        return getInscricao().getNome();
    }

    public String getParam(String key) {
        return paramBoleto.getString(key);
    }

    public Integer getParamAsInt(String key) {
        String param = getParam(key);
        return param == null ? null : Integer.valueOf(param);
    }

    public Long getParamAsLong(String key) {
        String param = getParam(key);
        return param == null ? null : Long.valueOf(param);
    }

    /**
     * Retorna uma parâmetro do arquivo de recursos usando {@code pattern} como
     * formato de entrada. O padrão utilizado deve estar de acordo com a
     * especificação da classe {@code java.text.SimpleDateFormat}.
     *
     * @param key A chave identificadora do parâmetro
     * @param pattern O formato do valor de entrada
     * @return A data
     */
    public Date getParamAsDate(String key, String pattern) {
        String param = getParam(key);
        if (param == null) {
            return null;
        }
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            return sdf.parse(param);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Retorna uma parâmetro do arquivo de recursos usando o formato canônico
     * (yyyy-MM-dd).
     *
     * @param key A chave identificadora do parâmetro
     * @return A data
     */
    public Date getParamAsDate(String key) {
        return getParamAsDate(key, "yyyy-MM-dd");
    }

    public String getConfig(String key) {
        return config.getString(key);
    }

    public Integer getConfigAsInt(String key) {
        String param = getConfig(key);
        return param == null ? null : Integer.valueOf(param);
    }

    public Long getConfigAsLong(String key) {
        String param = getConfig(key);
        return param == null ? null : Long.valueOf(param);
    }

    /**
     * Retorna uma parâmetro do arquivo de recursos usando {@code pattern} como
     * formato de entrada. O padrão utilizado deve estar de acordo com a
     * especificação da classe {@code java.text.SimpleDateFormat}.
     *
     * @param key A chave identificadora do parâmetro
     * @param pattern O formato do valor de entrada
     * @return A data
     */
    public Date getConfigAsDate(String key, String pattern) {
        String param = getConfig(key);
        if (param == null) {
            return null;
        }
        final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            return sdf.parse(param);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Retorna uma parâmetro do arquivo de recursos usando o formato canônico
     * (yyyy-MM-dd).
     *
     * @param key A chave identificadora do parâmetro
     * @return A data
     */
    public Date getConfigAsDate(String key) {
        return getConfigAsDate(key, "yyyy-MM-dd");
    }

    public Date getDataDocumento() {
        return dataDocumento;
    }

    public Date getDataProcessamento() {
        return dataProcessamento;
    }

    public long getNumeroDocumento() {
        InscricaoPagamento pgto = getInscricaoLazy().getInscricaoPagamentoList().get(0);
        Integer idEvento = getParamAsInt("boleto.evento.id");
        long numDoc =  idEvento * 10000000000000L + pgto.getNumeroDocumento() * 100000L
                + pgto.getInscricao().getNumero();
        closeInscricaoJpaController();
        return numDoc;
    }

    public long getNossoNumero() {
        return 24000000000000000L + getNumeroDocumento();
    }

    public Double getValorDocumento() {
        Double valor = getInscricaoLazy().getInscricaoPagamentoList()
                .get(0).getValorDocumento();
        closeInscricaoJpaController();
        return valor;
    }

    public long getValorDoctoAsLong() {
        return Math.round(getValorDocumento() * 100);
    }

    /**
     * Retorna o dígito verificador do nosso número.
     *
     * @return O DV do nosso número
     */
    public int getDvNossoNumero() {
        dvMod11.setValor(getNossoNumero());
        return dvMod11.getDigito();
    }

    public Date getDataVencimento() {
        Date dataVencto = getInscricaoLazy().getInscricaoPagamentoList()
                .get(0).getDataVencimento();
        closeInscricaoJpaController();
        return dataVencto;
    }

    public int getDvCampoLivre() {
        String nn = String.format("%017d", getNossoNumero());
        String cl = String.format("%06d%01d%3s%1d%3s%1d%9s",
                getParamAsInt("boleto.cedente.codigo"),
                getParamAsInt("boleto.cedente.dv"),
                nn.substring(2, 5),
                getParamAsInt("boleto.cobranca.tipo"),
                nn.substring(5, 8),
                getParamAsInt("boleto.cobranca.emissao"),
                nn.substring(8, 17));
        dvMod11.setValor(cl);
        return dvMod11.getDigito();
    }
    
    public int getFatorVencimento() {
        Date venc = getDataVencimento();
        long difMillis = venc.getTime() - dataBaseVencimento.getTimeInMillis();
        long difDias = difMillis / (24 * 60 * 60 * 1000L);
        return (int) difDias;
    }

    public String getCampoLivre() {
        String nn = String.format("%017d", getNossoNumero());
        return String.format("%06d%01d%3s%1d%3s%1d%9s%1d",
                getParamAsInt("boleto.cedente.codigo"),
                getParamAsInt("boleto.cedente.dv"),
                nn.substring(2, 5),
                getParamAsInt("boleto.cobranca.tipo"),
                nn.substring(5, 8),
                getParamAsInt("boleto.cobranca.emissao"),
                nn.substring(8, 17),
                getDvCampoLivre());
    }

    public int getDvCodigoBarras() {
        String base = String.format("%03d%1d%04d%010d%25s",
                getParamAsInt("boleto.banco.codigo"),
                getParamAsInt("boleto.moeda.codigo"),
                getFatorVencimento(),
                getValorDoctoAsLong(),
                getCampoLivre());
        dvMod11.setValor(base);
        dvMod11.setVariante(DVModulo11.DVM11_BARCODE);
        int dv = dvMod11.getDigito();
        dvMod11.setVariante(DVModulo11.DVM11_NORMAL);
        return dv;
    }

    public String getCodigoBarras() {
        return String.format("%03d%1d%1d%04d%010d%25s",
                getParamAsInt("boleto.banco.codigo"),
                getParamAsInt("boleto.moeda.codigo"),
                getDvCodigoBarras(),
                getFatorVencimento(),
                getValorDoctoAsLong(),
                getCampoLivre());
    }

    public String getLinhaDigitavel() {
        StringBuilder linha = new StringBuilder(54);
        String cb = getCodigoBarras();
        String base = cb.substring(0, 4) + cb.substring(19, 24);
        dvMod10.setValor(base);
        linha.append(base.substring(0, 5)).append('.').append(base.substring(5, 9))
                .append(dvMod10.getDigito()).append(' ');
        dvMod10.setValor(cb.substring(24, 34));
        linha.append(cb.substring(24, 29)).append('.').append(cb.substring(29, 34))
                .append(dvMod10.getDigito()).append(' ');
        dvMod10.setValor(cb.substring(34, 44));
        linha.append(cb.substring(34, 39)).append('.').append(cb.substring(39, 44))
                .append(dvMod10.getDigito()).append(' ');
        linha.append(cb.charAt(4)).append(' ');
        linha.append(cb.substring(5, 19));
        return linha.toString();
    }

    public List<String> getInstrucoes() {
        try {
            if (instrucoes == null) {
                Inscricao inscricao = getInscricaoLazy();
                instrucoes = new ArrayList<>();
                instrucoes.add(getParam("boleto.instrucao.titulo"));
                instrucoes.add("- " + inscricao.getCategoria().getDescricao());
                instrucoes.add("- Evento");
                StringBuilder instrucao = new StringBuilder(20);
                instrucao.append("- Minicursos:");
                for (InscricaoMinicurso imc: inscricao.getInscricaoMinicursoList()) {
                    instrucao.append(" ");
                    instrucao.append(imc.getMinicurso().getCodigo());
                }
                if (inscricao.getInscricaoMinicursoList().size() > 0)
                    instrucoes.add(instrucao.toString());
                
                instrucao = new StringBuilder(20);
                instrucao.append("- Visitas:");
                for (InscricaoVisita iv: inscricao.getInscricaoVisitaList()) {
                    instrucao.append(" ");
                    instrucao.append(iv.getVisita().getCodigo());
                }
                if (inscricao.getInscricaoVisitaList().size() > 0) {
                    instrucoes.add(instrucao.toString());
                }
                if (inscricao.getCamiseta() != null) {
                    instrucoes.add("- Camiseta do evento");
                }
                instrucoes.add(getParam("boleto.instrucao.naoReceber"));
            }
        } catch (Exception e) {
            log("getInstrucoes", e);
        } finally {
            closeInscricaoJpaController();
        }
        return instrucoes;
    }

    /**
     * Retorna o fuso horário padrão. Este valor é necessário, por exemplo, nos
     * conversores de data para que a hora seja convertida corretamente de
     * acordo com o fuso horário local.
     *
     * @return O fuso horário padrão
     */
    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }
}
