/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.faces.mngbeans;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Inscricao;
import java.io.File;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
import org.icefaces.ace.component.fileentry.FileEntryStatuses;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@RequestScoped
public class SubmissaoBean extends PageBean {

    private Inscricao inscricao;
    private Long cpf;
    private String fone;
    private String email;

    public SubmissaoBean() {

    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String submeterAction() {
        String result = null;
        InscricaoJpaController ctl = new InscricaoJpaController();
        try {
            inscricao = ctl.findByCPF(cpf);
            if (inscricao == null || !inscricao.getFone().equals(fone)
                    || !inscricao.getEmail().equals(email)) {
                error("Inscrição não localizada. Por favor, certifique-se de "
                        + "que os dados informados estão corretos.");
            } else {
                result = null;
            }
        } catch (Exception e) {
            log("", e);
            error("Erro na consulta: " + e.getLocalizedMessage());
        } finally {
            ctl.close();
        }
        return result;
    }
    
    public void fileEntryListener(FileEntryEvent event) {
        FileEntry fe = (FileEntry) event.getComponent();
        FileEntryResults results = fe.getResults();
        FileEntryResults.FileInfo fileInfo = null;
        try {
            fileInfo = results.getFiles().get(0);
            if (fileInfo.getContentType().equalsIgnoreCase("application/pdf")) {
                File file = fileInfo.getFile();
                File renFile = new File(file.getParent(), 
                    String.format("%05d-001.pdf", inscricao.getNumero()));
                if (renFile.exists()) {
                    renFile.delete();
                }
                file.renameTo(renFile);
            } else {
                if (fileInfo != null) {
                    fileInfo.updateStatus(FileEntryStatuses.INVALID_CONTENT_TYPE,
                            true, true);
                }
            }
        } catch (Exception e) {
            log("", e);
            error("Falha na submissão do arquivo: " + e.getLocalizedMessage());
        }
    }
}
