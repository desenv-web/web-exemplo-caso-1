package evento.faces.mngbeans;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Inscricao;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@RequestScoped
public class ConsultaArtigosBean extends  PageBean {
    private static final long serialVersionUID = 1L;
    DataModel<Inscricao> inscricoesDataModel;

    public ConsultaArtigosBean() {
    }

    public List<Inscricao> getInscricoes() {
        InscricaoJpaController ctl = new InscricaoJpaController();
        return ctl.findByArtigos();
    }
    
    public DataModel<Inscricao> getInscricoesDataModel() {
        if (inscricoesDataModel == null)
            inscricoesDataModel = new ListDataModel<>(getInscricoes());
        return inscricoesDataModel;
    }
    
}
