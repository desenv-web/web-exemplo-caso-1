package evento.faces.mngbeans;

import evento.persistence.controller.ArtigoJpaController;
import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Artigo;
import evento.persistence.entity.ArtigoPK;
import evento.persistence.entity.Inscricao;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.codec.digest.DigestUtils;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
import org.icefaces.ace.component.fileentry.FileEntryStatuses;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class ArtigoUploadBean extends PageBean {
    
    public static final String MIME_VALIDO = "application/pdf";

    private Inscricao inscricao;
    private Artigo artigo = new Artigo();
    private Long cpf;
    private String fone;
    private String email;

    /**
     * Creates a new instance of ArtigoUploadBean
     */
    public ArtigoUploadBean() {
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Inscricao getInscricao() {
        if (cpf != null && inscricao == null) {
            InscricaoJpaController ctl = new InscricaoJpaController();
            inscricao = ctl.findByCPF(cpf);
        }
        return inscricao;
    }

    public Artigo getArtigo() {
        return artigo;
    }

    public void setArtigo(Artigo artigo) {
        this.artigo = artigo;
    }
    
    private boolean validaSituacao() {
        boolean valido = inscricao.getSituacao().equals(InscricaoJpaController.SITUACAO_CONFIRMADO); 
        if (!valido) {
            error("Sua inscrição ainda não foi confirmada: submissão de trabalhos indisponível");
        }
        return valido;
    }
    
    public boolean isMaxArtigosSubmetidos() {
        return inscricao.getArtigoList().size() >= 1;
    }
    
    public boolean isInscricaoConfirmada() {
        return getInscricao().getSituacao().getCodigo()
            .equals(InscricaoJpaController.SITUACAO_CONFIRMADO.getCodigo());
    }
    
    private boolean validaMaxArtigos() {
        boolean valido = !isMaxArtigosSubmetidos();
        if (!valido) {
            error("Você já submeteu o número máximo de trabalhos permitidos");
        }
        return valido;
    }
    
    private boolean validaInscricao() {
        boolean valida = false;
        try {
            inscricao = getInscricao();
            valida = inscricao != null && inscricao.getFone().equals(fone)
                        && inscricao.getEmail().equalsIgnoreCase(email);
            if (!valida) {
                error("Inscrição não localizada. Por favor, certifique-se de "
                        + "que os dados informados estão corretos.");
                inscricao = null;
            }
        } catch (Throwable t) {
            inscricao = null;
            log("validaInscricao", t);
            throw t;
        }
        return valida;
    }
    
    private boolean validaTipoArquivo(FileEntryResults.FileInfo fileInfo) {
        // TODO: Verificar o conteúdo do arquivo para assegurar que seja
        //       realmente um PDF (arquivo começa com o string %PDF).
        //       De forma mais genérica, implementar verificação de outros
        //       tipos de arquivos.
        //       (*) Teste do mime type não se mostrou confiável pois depende
        //           do envio correto do Content-Type pelo navegador
        boolean valido = fileInfo.getFileName().toLowerCase().endsWith(".pdf");
        if (!valido) {
            fileInfo.updateStatus(FileEntryStatuses.INVALID, true);
        }
        return valido;
    }

    private boolean validaSubmissao(FileEntryResults.FileInfo fileInfo) {
        return validaTipoArquivo(fileInfo);
    }
    
    private void resetUpload() {
        artigo.setTitulo(null);
        artigo.setResumo(null);
        inscricao = null;
    }
    
    public String getArtigoHash() {
        String base = String.format("%011d%s", cpf,
                getExternalContext().getSessionId(false));
        return DigestUtils.sha1Hex(base);
    }
    
    private void adicionaArtigo(FileEntryResults.FileInfo info) {
        ArtigoJpaController ctl = new ArtigoJpaController();
        artigo.setArtigoPK(new ArtigoPK(inscricao.getNumero(), inscricao.getArtigoList().size()+1));
        artigo.setInscricao(inscricao);
        artigo.setArquivo(info.getFileName());
        ctl.persist(artigo);
        inscricao = ctl.getEntityManager().merge(inscricao);
    }
    
    public String uploadListener(FileEntryEvent event) {
        Date agora = new Date();
        FileEntry fe = (FileEntry) event.getComponent();
        FileEntryResults results = fe.getResults();
        List<FileEntryResults.FileInfo> filesInfo = results.getFiles();
        for (FileEntryResults.FileInfo info : filesInfo) {
            if (validaSubmissao(info)) {
                adicionaArtigo(info);
            } else {
                error(String.format("Erro: '%s' não pode ser submetido", info.getFileName()));
            }
        }
        resetUpload();
        return null;
    }

    public void validaInscricaoAction() {
        boolean valido = validaInscricao() && validaSituacao() &&  validaMaxArtigos();
        if (valido) {
            artigo = new Artigo();
            artigo.setInscricao(inscricao);
        } else {
            inscricao = null;
        }
    }
    
    public void submeterTrabalhoAction() {
        
    }
}
