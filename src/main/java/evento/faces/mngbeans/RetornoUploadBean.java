package evento.faces.mngbeans;

import evento.cobranca.ProcessaRetorno;
import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Artigo;
import evento.persistence.entity.Inscricao;
import evento.persistence.entity.InscricaoMinicurso;
import evento.persistence.entity.InscricaoVisita;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
import org.icefaces.ace.component.fileentry.FileEntryStatuses;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@RequestScoped
public class RetornoUploadBean extends PageBean {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(ProcessaRetorno.class.getName());

    public RetornoUploadBean() {

    }
    
    private ProcessaRetorno processaArquivo(FileEntryResults.FileInfo info) {
        ProcessaRetorno pr = new ProcessaRetorno(info.getFile());
        try {
            if (info.getStatus().isSuccess())
                pr.processa();
        } catch (Throwable t) {
            info.updateStatus(FileEntryStatuses.INVALID, true);
        }
        return pr;
    }

    /**
     * Atualiza o status das inscrições para cancelado (3) quando o pagamento
     * não for registrado pelo banco após 4 dias da data da inscrição.
     * @param dataRetorno Data do retorno mais recente
     */
    public void updateStatusNaoPago(Date dataRetorno) {
        InscricaoJpaController ctl = new InscricaoJpaController(true);
        List<Inscricao> inscricoes = ctl.findBySituacao(1); // pendentes
        for (Inscricao insc: inscricoes) {
            Date dataVcto = insc.getInscricaoPagamentoList().get(0).getDataVencimento();
            GregorianCalendar dataLimiteConfirmacao = new GregorianCalendar();
            dataLimiteConfirmacao.setTime(dataVcto);
            dataLimiteConfirmacao.add(Calendar.DAY_OF_MONTH, 4);
            if (dataRetorno.after(dataLimiteConfirmacao.getTime())) {
                cancelaInscricao(insc);
                ctl.persist(insc);
                logger.info(String.format("INSCRIÇÃO %05d CANCELADA", insc.getNumero()));
            }
        }
        ctl.close();
    }

    private void cancelaInscricao(Inscricao inscricao) {
        inscricao.setSituacao(InscricaoJpaController.SITUACAO_CANCELADO);
        for (InscricaoMinicurso im: inscricao.getInscricaoMinicursoList()) {
            im.setSituacao(InscricaoJpaController.SITUACAO_CANCELADO);
        }
        for (InscricaoVisita iv: inscricao.getInscricaoVisitaList()) {
            iv.setSituacao(InscricaoJpaController.SITUACAO_CANCELADO);
        }
    }
    
    public String uploadListener(FileEntryEvent event) {
        Date dataRetorno = new Date(0);
        FileEntry fe = (FileEntry) event.getComponent();
        FileEntryResults results = fe.getResults();
        List<FileEntryResults.FileInfo> filesInfo = results.getFiles();
        for (FileEntryResults.FileInfo info: filesInfo) {
            ProcessaRetorno pr = processaArquivo(info);
            if (dataRetorno.before(pr.getDataRetorno()))
                dataRetorno = pr.getDataRetorno();
        }
        updateStatusNaoPago(dataRetorno);
        return null;
    }

}
