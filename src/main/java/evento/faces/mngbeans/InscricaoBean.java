package evento.faces.mngbeans;

import evento.mail.Mail;
import javax.faces.bean.ManagedBean;
import evento.persistence.controller.CategoriaJpaController;
import evento.persistence.controller.CronogramaJpaController;
import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.controller.InscricaoMinicursoJpaController;
import evento.persistence.controller.InscricaoVisitaJpaControler;
import evento.persistence.controller.MinicursoJpaController;
import evento.persistence.controller.VisitaJpaController;
import evento.persistence.entity.Categoria;
import evento.persistence.entity.Cronograma;
import evento.persistence.entity.Inscricao;
import evento.persistence.entity.InscricaoMinicurso;
import evento.persistence.entity.InscricaoMinicursoPK;
import evento.persistence.entity.InscricaoPagamento;
import evento.persistence.entity.InscricaoVisita;
import evento.persistence.entity.InscricaoVisitaPK;
import evento.persistence.entity.Minicurso;
import evento.persistence.entity.Visita;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.bean.ViewScoped;
import utfpr.faces.support.RequestBean;

@ManagedBean
@ViewScoped
public class InscricaoBean extends RequestBean {
    private static final long serialVersionUID = 1L;
    
    private Integer numeroInscricao;
    private Inscricao inscricao = new Inscricao();
    private Integer categoriaSelecionada = null;
    private boolean opcaoParticipacao;
    private Boolean opcaoOutroMinicurso = false;
    private Boolean[] checkVisitas = { false, false, false };
    private Boolean[] checkMinicursos = { false, false, false };
    private Boolean[] checkCamiseta = { false, false, false, false };
    private Boolean[] categoriasRadio = { false, false };
    private final Cronograma cronograma = new CronogramaJpaController().getCronograma();
    
    
    // em caso de erro no acesso ao banco de dados, retornarÃ¡ listas vazias
    private List<Categoria> categorias = new ArrayList<>(0);
    private List<Minicurso> minicursos = new ArrayList<>(0);
    private List<Visita> visitas = new ArrayList<>(0);

    private static final GregorianCalendar dataLimiteDesconto =
            new GregorianCalendar(2014, Calendar.OCTOBER, 27);
    private static final GregorianCalendar dataFinalInscricao = new GregorianCalendar();
    public static final String valorPattern = "[R$ 0.00]";
    
    private static final double VALOR_CAMISETA = 20.00; 

    private static final int VAGAS_EVENTO = 130;
    
    public InscricaoBean() {
        dataFinalInscricao.setTime(cronograma.getData3());
        ConsultaBean consultaBean = (ConsultaBean) getBean("consultaBean");
        if (consultaBean != null && consultaBean.getInscricao() != null) {
            inscricao = consultaBean.getInscricao();
            inicializaConsulta();
        }
    }

    private Date getDataInscricao() {
        if (inscricao == null || inscricao.getDataHora() == null) {
            return new Date();
        } else {
            return inscricao.getDataHora();
        }
    }
    
    private void inicializaConsulta() {
        categoriaSelecionada = inscricao.getCategoria().getCodigo();
        categoriasRadio[categoriaSelecionada-1] = true;
        for (InscricaoMinicurso imc: inscricao.getInscricaoMinicursoList()) {
            checkMinicursos[imc.getMinicurso().getCodigo()-1] = true;
        }
        
        for (InscricaoVisita iv: inscricao.getInscricaoVisitaList()) {
            checkVisitas[iv.getVisita().getCodigo()-1] = true;
        }
        
        if (inscricao.getCamiseta() != null) {
            switch (inscricao.getCamiseta()) {
                case "P": checkCamiseta[0] = true; break;
                case "M": checkCamiseta[1] = true; break;
                case "G": checkCamiseta[2] = true; break;
                case "BL": checkCamiseta[3] = true; break;
            }
        }
        opcaoParticipacao = inscricao.isParticipacao();
    }

    public Inscricao getInscricao() {
        return inscricao;
    }

    public Integer getNumeroInscricao() {
        return numeroInscricao;
    }

    public void setNumeroInscricao(Integer numeroInscricao) {
        this.numeroInscricao = numeroInscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    public GregorianCalendar getDataLimiteDesconto() {
        return dataLimiteDesconto;
    }

    public String getValorPattern() {
        return valorPattern;
    }

    public Cronograma getCronograma() {
        return cronograma;
    }

    public Boolean isDesconto() {
        GregorianCalendar dataBase = new GregorianCalendar();
        if (isPersisted()) dataBase.setTime(inscricao.getDataHora());
        return dataBase.before(dataLimiteDesconto);
    }

    public Boolean isCategoriaDefinida() {
        return getCategoriaSelecionada() != null;
    }

    public Categoria getCategoriaInscricao() {
        return isCategoriaDefinida() ?
            getCategorias().get(categoriaSelecionada-1) : null;
    }

    public Double getValorParticipacao() {
        Cronograma cron = getCronograma();
        Double valor = 0.0;
        if (isCategoriaDefinida() && inscricao.isParticipacao()) {
            valor = getDataInscricao().before(cron.getData2()) ?
                    getInscricao().getCategoria().getValor1Inscricao() :
                    getInscricao().getCategoria().getValor2Inscricao();
        }
        return valor;
    }

    public Double getValorMinicurso() {
        Cronograma cron = getCronograma();
        Double valor = 0.0;
        if (isCategoriaDefinida() && !inscricao.isParticipacao()) {
            valor = getDataInscricao().before(cron.getData2()) ?
                    getInscricao().getCategoria().getValor1Minicurso():
                    getInscricao().getCategoria().getValor2Minicurso();
        }
        return valor;
    }

    public Double getValorVisita() {
        Cronograma cron = getCronograma();
        Double valor = 0.0;
        if (isCategoriaDefinida()) {
            valor = getDataInscricao().before(cron.getData2()) ?
                    getInscricao().getCategoria().getValor1Visita():
                    getInscricao().getCategoria().getValor2Visita();
        }
        return valor;
    }
    
    public String getTamanhoCamiseta() {
        String tamanho = null;
        for (int i = 0; i < checkCamiseta.length && tamanho == null; i++) {
            if (checkCamiseta[i]) {
                switch (i) {
                    case 0: tamanho = "P"; break;
                    case 1: tamanho = "M"; break;
                    case 2: tamanho = "G"; break;
                    case 3: tamanho = "BL"; break;
                }
            }
        }
        return tamanho;
    }
    
    public Double getValorCamiseta() {
        return getTamanhoCamiseta() == null ? 0.0 : VALOR_CAMISETA;
    }

    public int getNumMinicursos() {
        int n = 0;
        for (Boolean c: checkMinicursos) {
            if (c) n++;
        }
        return n;
    }
    
    public int getNumVisitas() {
        int n = 0;
        for (Boolean c: checkVisitas) {
            if (c) n++;
        }
        return n;
    }

    public Double getValorTotalMinicursos() {
        Double valor = null;
        if (getValorMinicurso()!= null) {
            valor = getValorMinicurso() * getNumMinicursos();
        }
        return valor;
    }

    public Double getValorTotalVisitas() {
        Double valor = null;
        if (getValorVisita() != null) {
            valor = getValorVisita() * getNumVisitas();
        }
        return valor;
    }

    public Double getValorTotalInscricao() {
        return getValorParticipacao() + getValorTotalMinicursos()
                + getValorTotalVisitas() + getValorCamiseta();
    }

    public Categoria getCategoriaSelecionada() {
        Categoria c = null;
        for (int i = 0; i < 2; i++) {
            if (categoriasRadio[i]) {
                c = getCategorias().get(i); 
            }
        }
        return c;
    }

    public void setCategoriaSelecionada(Integer categoriaSelecionada) {
        this.categoriaSelecionada = categoriaSelecionada;
    }

    public Boolean[] getCheckVisitas() {
        return checkVisitas;
    }

    public void setCheckVisitas(Boolean[] checkVisitas) {
        this.checkVisitas = checkVisitas;
    }

    public Boolean[] getCheckMinicursos() {
        return checkMinicursos;
    }

    public void setCheckMinicursos(Boolean[] checkMinicursos) {
        this.checkMinicursos = checkMinicursos;
    }

    public Boolean[] getCheckCamiseta() {
        return checkCamiseta;
    }

    public void setCheckCamiseta(Boolean[] checkCamiseta) {
        this.checkCamiseta = checkCamiseta;
    }
    
    public boolean isOpcaoParticipacao() {
        return opcaoParticipacao;
    }

    public void setOpcaoParticipacao(boolean opcaoParticipacao) {
        this.opcaoParticipacao = opcaoParticipacao;
    }

    public Boolean isOpcaoOutroMinicurso() {
        return opcaoOutroMinicurso;
    }

    public void setOpcaoOutroMinicurso(Boolean opcaoOutroMinicurso) {
        this.opcaoOutroMinicurso = opcaoOutroMinicurso;
    }

    public List<Categoria> getCategorias() {
        try {
            if (categorias.isEmpty()) {
                CategoriaJpaController ctl = new CategoriaJpaController();
                categorias = ctl.findAll();
            }
        } catch (Exception e) {
            log("", e);
            error(e.getLocalizedMessage());
        }
        return categorias;
    }

    public Boolean[] getCategoriasRadio() {
        return categoriasRadio;
    }
    
    public List<Minicurso> getMinicursos() {
        try {
            if (minicursos.isEmpty()) {
                MinicursoJpaController ctl = new MinicursoJpaController();
                minicursos = ctl.findAll();
            }
        } catch (Exception e) {
            log("", e);
            error(e.getLocalizedMessage());
        }
        return minicursos;
    }

    public List<Visita> getVisitas() {
        try {
            if (visitas.isEmpty()) {
                VisitaJpaController ctl = new VisitaJpaController();
                visitas = ctl.findAll();
            }
        } catch (Exception e) {
            log("", e);
            error(e.getLocalizedMessage());
        }
        return visitas;
    }

    public int getVagasDiscponiveisEvento() {
        InscricaoJpaController ctl = new InscricaoJpaController();
        return VAGAS_EVENTO - ctl.countInscritos();
    }
    
    public Boolean isVagaDisponivelEvento() {
        return getVagasDiscponiveisEvento() > 0;
    }
    
    public Boolean isVagaDisponivelMinicurso(Minicurso mc) {
        InscricaoMinicursoJpaController ctl = new InscricaoMinicursoJpaController();
        return ctl.countInscritos(mc.getCodigo()) < mc.getVagas();
    }
    
    public int getVagasDisponiveisMinicurso(int minicurso) {
        try {
            InscricaoMinicursoJpaController ctl = new InscricaoMinicursoJpaController();
            return getMinicursos().get(minicurso-1).getVagas() - ctl.countInscritos(minicurso);
        } catch (Exception e) {
            log("getVagasDisponiveisMinicurso", e);
            return -1;
        }
    }

    public Boolean isVagaDisponivelVisita(Visita visita) {
        InscricaoVisitaJpaControler ctl = new InscricaoVisitaJpaControler();
        return ctl.countInscritos(visita.getCodigo()) < visita.getVagas();
    }

    public int getVagasDisponiveisVisita(int visita) {
        try {
            InscricaoVisitaJpaControler ctl = new InscricaoVisitaJpaControler();
            return getVisitas().get(visita-1).getVagas() - ctl.countInscritos(visita);
        } catch (Exception e) {
            log("getVagasDisponiveisVisita", e);
            return -1;
        }
    }

    private int getPrazoPgto(GregorianCalendar dataBase) {
        int dow = dataBase.get(Calendar.DAY_OF_WEEK);
        int prazo = 4; // Calendar.SATURDAY
        switch (dow) {
            case Calendar.MONDAY:
            case Calendar.TUESDAY:
            case Calendar.SUNDAY:
                prazo = 3;
                break;
            case Calendar.WEDNESDAY:
            case Calendar.THURSDAY:
            case Calendar.FRIDAY:
                prazo = 5;
                break;
        }
        return prazo;
    }

    public Date dataVencimento(Date base) {
        GregorianCalendar vcto = new GregorianCalendar();
        if (base != null) {
            vcto.setTime(base);
        }
        int prazoPgto = getPrazoPgto(vcto);
        vcto.add(Calendar.DAY_OF_MONTH, prazoPgto);
        return vcto.getTime();
    }

    public void inicializaInscricao() {
        inscricao.setCategoria(getCategoriaSelecionada());
        inscricao.setCamiseta(getTamanhoCamiseta());
        inscricao.setSituacao(InscricaoJpaController.SITUACAO_PENDENTE);
        if (getNumMinicursos() > 0) {
            List<InscricaoMinicurso> imcList = new ArrayList<>();
            for (int i = 0; i < checkMinicursos.length; i++) {
                if (checkMinicursos[i]) {
                    InscricaoMinicurso imc = new InscricaoMinicurso();
                    imc.setInscricaoMinicursoPK(new InscricaoMinicursoPK());
                    imc.setSituacao(InscricaoJpaController.SITUACAO_PENDENTE);
                    imc.setMinicurso(getMinicursos().get(i));
                    imc.setInscricao(inscricao);
                    imcList.add(imc);
                }
            }
            inscricao.setInscricaoMinicursoList(imcList);
        }
        if (getNumVisitas() > 0) {
            List<InscricaoVisita> ivList = new ArrayList<>();
            for (int i = 0; i < checkVisitas.length; i++) {
                if (checkVisitas[i]) {
                    InscricaoVisita iv = new InscricaoVisita();
                    iv.setInscricaoVisitaPK(new InscricaoVisitaPK());
                    iv.setSituacao(InscricaoJpaController.SITUACAO_PENDENTE);
                    iv.setVisita(getVisitas().get(i));
                    iv.setInscricao(inscricao);
                    ivList.add(iv);
                }
            }
            inscricao.setInscricaoVisitaList(ivList);
        }
        
        List<InscricaoPagamento> pgtos = new ArrayList<>(1);
        InscricaoPagamento pgto = new InscricaoPagamento();
        pgto.setInscricao(inscricao);
        pgto.setValorDocumento(getValorTotalInscricao());
        Date agora = new Date();
        pgto.setDataHoraProcessamento(agora);
        pgto.setDataVencimento(dataVencimento(agora));
        pgtos.add(pgto);
        inscricao.setInscricaoPagamentoList(pgtos);
    }

    public Boolean isCpfExistente() {
        InscricaoJpaController ctl = new InscricaoJpaController();
        return ctl.findByCPF(inscricao.getCpf()) != null;
    }

    public boolean isPersisted() {
        return inscricao != null && inscricao.getNumero() != null;
    }

    private void novaInscricao() {
        InscricaoJpaController ctl = new InscricaoJpaController();
        inscricao = ctl.persist(inscricao);
    }

    Boolean validaVagasEvento() {
        return isVagaDisponivelEvento();
    }
    
    private Boolean validaVagasVisitas() {
        Boolean valido = true;
        if (getNumVisitas() > 0) {
            for (int i = 0; i < checkVisitas.length; i++) {
                if (checkVisitas[i]) {
                    Visita v = getVisitas().get(i);
                    if (!isVagaDisponivelVisita(v)) {
                        error("Visita a " + v.getDescricao() + " não tem vagas disponíveis");
                        checkVisitas[i] = false;
                        valido = false;
                    }
                }
            }
        }
        return valido;
    }
    
    private Boolean validaVagasMinicursos() {
        Boolean valido = true;
        if (getNumMinicursos()> 0) {
            for (int i = 0; i < checkMinicursos.length; i++) {
                if (checkMinicursos[i]) {
                    Minicurso mc = getMinicursos().get(i);
                    if (!isVagaDisponivelMinicurso(mc)) {
                        error("Minicurso " + mc.getDescricao() + " não tem vagas dispoíveis");
                        checkMinicursos[i] = false;
                        valido = false;
                    }
                }
            }
        }
        return valido;
    }

    private Boolean validaCPF() {
        Boolean valido = !isCpfExistente();
        if (!valido) {
            error("Este CPF já¡ está inscrito no evento");
        }
        return valido;
    }

    private String getTextoEmailConfirmacao() {
        StringBuilder msg = new StringBuilder(256);
        msg.append(String.format("Prezado(a) %s,\n", inscricao.getNome()));
        msg.append("Sua solicitação de inscrição foi registrada.\n\n");
        msg.append(String.format("Número: %05d\n", inscricao.getNumero()));
        msg.append(String.format("CPF: %09d\n", inscricao.getCpf()));
        msg.append(String.format("Endereço: %s\n", inscricao.getEndereco()));
        msg.append(String.format("          %s\n", inscricao.getBairro()));
        msg.append(String.format("          %08d %s %s\n",
                inscricao.getCep(), inscricao.getCidade(), inscricao.getEstado()));
        msg.append(String.format("Fone: %s\n\n", inscricao.getFone()));
        if (inscricao.isParticipacao()) {
            msg.append("   - Seminário\n");
        }
        
        Boolean titulo = true;
        for (InscricaoVisita iv: inscricao.getInscricaoVisitaList()) {
            if (titulo) msg.append("   - Visitas técnicas:\n");
            msg.append(            "       ");
            titulo = false;
            msg.append(iv.getVisita().getDescricao()).append("\n");
        }
        msg.append("\n\n");
        
        titulo = true;
        for (InscricaoMinicurso imc: inscricao.getInscricaoMinicursoList()) {
            if (titulo) msg.append("   - Minicursos:\n");
            msg.append(            "       ");
            titulo = false;
            msg.append(imc.getMinicurso().getDescricao()).append("\n");
        }
        
        msg.append("\n\n");
        msg.append(String.format("\nValor total da inscrição: R$ %.2f\n\n", getValorTotalInscricao()));

        msg.append("Consultar inscrição:\n");
        msg.append("http://www.ct.utfpr.edu.br/ontobras/consulta.xhtml\n\n");
        msg.append("Emissão do boleto para pagamento:\n");
        msg.append(String.format("http://www.ct.utfpr.edu.br/ontobras/boleto.xhtml?npgto=%d&ninsc=%d\n",
                inscricao.getInscricaoPagamentoList().get(0).getNumeroDocumento(), inscricao.getNumero()));
      
        return msg.toString();
    }

    private void enviaEmailConfirmacao() {
        InscricaoJpaController ctl = null;
        try {
            Mail mail = new Mail();
            ctl = new InscricaoJpaController(true);
            inscricao = ctl.findByNumero(inscricao.getNumero());
            inscricao.getInscricaoPagamentoList().size(); // load lazy collection
            mail.send(inscricao.getEmail(), getTextoEmailConfirmacao());
        } catch (Exception e) {
            log("enviaEmailConfirmacao", e);
        } finally {
            if (ctl != null) ctl.close();
        }
    }

    private Boolean validaVagas() {
        Boolean valido = validaVagasEvento();
        valido = valido && validaVagasMinicursos() && validaVagasVisitas();
        return valido;
    }
    
    private Boolean validaVisitas() {
        return true;
    }

    private Boolean validaMinicursos() {
        return true;
    }
    
    private boolean validaCategoria() {
        boolean valido = categoriasRadio[0] || categoriasRadio[1];
        if (!valido) {
            error("Selecione a categoria de inscrição");
        }
        return valido;
    }
    
    private boolean validaInscricao() {
        boolean valido = inscricao.isParticipacao() || getNumMinicursos() > 0;
        if (!valido) {
            error("Selecione inscrição no evento e/ou 1 ou mais minicursos");
        }
        return valido;
    }

    public String getMensagemSituacao() {
        String msg = null;
        switch (inscricao.getSituacao().getCodigo()) {
            case 1:
                msg = String.format("Pendente. Vencimento em %1$td/%1$tm/%1$tY.",
                    inscricao.getInscricaoPagamentoList().get(0).getDataVencimento());
                break;
            case 2:
                msg = String.format("Confirmada. Vencimento em %1$td/%1$tm/%1$tY. Pago em %2$td/%2$tm/%2$tY.",
                    inscricao.getInscricaoPagamentoList().get(0).getDataVencimento(),
                    inscricao.getInscricaoPagamentoList().get(0).getDataPagamento());
                break;
            case 3:
                msg = String.format("Cancelada. Vencimento em %1$td/%1$tm/%1$tY.",
                    inscricao.getInscricaoPagamentoList().get(0).getDataVencimento());
                break;
        }
        return msg;
    }

    public void confirmarAction() {
        Date agora = new Date();
        if (!agora.before(dataFinalInscricao.getTime())) {
            error("Inscriões encerradas");
            return;
        }
        try {
            inicializaInscricao();
            if (validaCPF() && validaInscricao() && validaCategoria()
                    && validaVisitas() && validaMinicursos() && validaVagas()) {
                novaInscricao();
                info("Inscrição solicitada. Pendente de pagamento da taxa.");
                enviaEmailConfirmacao();
            }
        } catch (Exception e) {
            log("", e);
            error("Não foi possível completar a inscrição: " + e.getLocalizedMessage());
        }
    }

}
