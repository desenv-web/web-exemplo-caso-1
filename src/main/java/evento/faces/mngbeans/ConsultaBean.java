/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.faces.mngbeans;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.entity.Inscricao;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@ManagedBean
@RequestScoped
public class ConsultaBean extends PageBean {

    private Inscricao inscricao;
    private Long cpf;
    private String fone;
    private String email;

    public ConsultaBean() {

    }

    public Inscricao getInscricao() {
        return inscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private void loadLazy() {
        inscricao.getInscricaoMinicursoList().size();
        inscricao.getInscricaoPagamentoList().size();
        inscricao.getInscricaoVisitaList().size();
    }

    public String consultaAction() {
        String result = null;
        InscricaoJpaController ctl = new InscricaoJpaController(true);
        try {
            inscricao = ctl.findByCPF(cpf);
            if (inscricao == null || !inscricao.getFone().equals(fone)
                    || !inscricao.getEmail().equals(email)) {
                error("Inscrição não localizada. Por favor, certifique-se de "
                        + "que os dados informados estão corretos.");
            } else {
                loadLazy();
                result = "inscricao";
            }
        } catch (Exception e) {
            log("consultaAction", e);
            error("Erro na consulta: " + e.getLocalizedMessage());
        } finally {
            ctl.close();
        }
        return result;
    }
}
