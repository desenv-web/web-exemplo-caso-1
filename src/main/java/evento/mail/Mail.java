/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.mail;

import com.sun.mail.util.MailSSLSocketFactory;
import java.security.GeneralSecurityException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Wilson
 */
public class Mail {

    public Mail() {
    }

    public Session getSession() throws GeneralSecurityException, NamingException {
        Context initCtx = new InitialContext();
        Session session = (Session) initCtx.lookup("java:/mail/cogeti");
        Properties props = session.getProperties();
        MailSSLSocketFactory msf = new MailSSLSocketFactory();
        msf.setTrustAllHosts(true);
        props.put("mail.smtp.socketFactory", msf);
        session.setDebug(true);
        return session;
    }

    public void send(String to, String texto) {
        try {
            Session session = getSession();
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("naoresponder@email.com.br"));
            InternetAddress[] replyTo = { new InternetAddress("reply@email.com.br") };
            message.setReplyTo(replyTo);
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Evento Exemplo Caso 1");
            message.setContent(texto, "text/plain; charset=ISO-8859-1");
            Transport transp = session.getTransport();
            transp.connect("usuario-de-email", "senha-do-usuario-de-email");
            transp.sendMessage(message, new InternetAddress[] {new InternetAddress(to)});
            transp.close();
        } catch (Exception ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
