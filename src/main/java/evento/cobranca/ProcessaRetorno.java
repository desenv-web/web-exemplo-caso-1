package evento.cobranca;

import evento.persistence.controller.InscricaoJpaController;
import evento.persistence.controller.JpaController;
import evento.persistence.entity.Inscricao;
import evento.persistence.entity.InscricaoMinicurso;
import evento.persistence.entity.InscricaoPagamento;
import evento.persistence.entity.InscricaoVisita;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import utfpr.file.text.cnab.Cnab240File;
import utfpr.file.text.cnab.Cnab240Parser;

/**
 * @author UTFPR
 * @author Wilson Horstmeyer Bogado
 */
public class ProcessaRetorno {

    private static final Logger logger = Logger.getLogger(ProcessaRetorno.class.getName());
    private final File retorno;

    private static final int CODIGO_EVENTO = 6;
    private static Date dataRetorno;
    private static int evento;
    private static int numeroInscricao;
    private static int numeroPgto;
    private static Date dataPagamento;
    private static int bancoCobrador;
    private static double valorTaxas;
    private static double valorPago;
    private static Date dataHoraConfirmacao;

    public ProcessaRetorno(File retorno) {
        this.retorno = retorno;
    }

    public boolean validaPagamento(InscricaoPagamento pgto) {
        if (pgto ==  null) {
            logger.log(Level.SEVERE, String.format("Pagamento %05d não localizado.", numeroPgto));
        } else if (pgto.getInscricao().getNumero() != numeroInscricao) {
            logger.log(Level.SEVERE, String.format("Pagamento %05d: inscrição %05d não localizada.", numeroPgto, numeroInscricao));
        }
        boolean valido = pgto != null && pgto.getNumeroDocumento() == numeroPgto;
        if (valido) {
            double difPgto = valorPago - pgto.getValorDocumento();
            if (difPgto >= 0.005) {
                logger.log(Level.WARNING, String.format("Pagamento %05d, inscrição %05d: pagamento a maior, devido R$ %.2f, pago R$ %.2f.",
                        numeroPgto, numeroInscricao, pgto.getValorDocumento(), valorPago));
            } else if (difPgto < -0.005) {
                valido = false;
                logger.log(Level.WARNING, String.format("INSCRIÇÃO NÃO CONFIRMADA, pagamento %05d, inscrição %05d: pagamento a menor, devido R$ %.2f, pago R$ %.2f.",
                        numeroPgto, numeroInscricao, pgto.getValorDocumento(), valorPago));
            }
        }
        return valido;
    }

    public void atualizaPagamento() {
        JpaController ctl = new JpaController(true);
        EntityManager em = ctl.getEntityManager();
        try {
            InscricaoPagamento pgto = em.find(InscricaoPagamento.class, numeroPgto);
            if (!validaPagamento(pgto)) return;
            pgto.setDataPagamento(dataPagamento);
            pgto.setValorPago(valorPago);
            pgto.setValorTarifa(valorTaxas);
            if (bancoCobrador > 0) pgto.setBancoPagamento(bancoCobrador);
            pgto.setDataRetorno(dataRetorno);
            pgto.setDataHoraConfirmacao(dataHoraConfirmacao);
            Inscricao inscricao = pgto.getInscricao();
            inscricao.setSituacao(InscricaoJpaController.SITUACAO_CONFIRMADO);
            
            List<InscricaoMinicurso> imcList = inscricao.getInscricaoMinicursoList();
            if (imcList != null) {
                for (InscricaoMinicurso imc: imcList) {
                    imc.setSituacao(InscricaoJpaController.SITUACAO_CONFIRMADO);
                }
            }

            List<InscricaoVisita> ivList = inscricao.getInscricaoVisitaList();
            if (ivList != null) {
                for (InscricaoVisita iv: ivList) {
                    iv.setSituacao(InscricaoJpaController.SITUACAO_CONFIRMADO);
                }
            }
            
            em.getTransaction().begin();
            em.persist(pgto);
            em.getTransaction().commit();
        } finally {
            ctl.close();
        }
    }

    public void processaHeader(Cnab240Parser parser) {
        dataRetorno = parser.getDateField(144, 151);
    }

    public Date getDataRetorno() {
        return dataRetorno;
    }
    
    public void processaHeaderLote(Cnab240Parser parser) {
    }

    public void processaSegmento(Cnab240Parser parser) {
        String segmento = parser.getField(14, 14);
        switch (segmento) {
            case Cnab240Parser.SEGMENTO_T:
                evento = parser.getIntField(42, 43);
                if (evento != CODIGO_EVENTO) return;
                numeroInscricao = parser.getIntField(52, 56);
                numeroPgto = parser.getIntField(47, 51);
                bancoCobrador = parser.getIntField(97, 99);
                valorTaxas = parser.getLongField(199, 213) / 100.0;
                break;
            case Cnab240Parser.SEGMENTO_U:
                if (evento != CODIGO_EVENTO) return;
                valorPago = parser.getLongField(78, 92) / 100.0;
                dataPagamento = parser.getDateField(138, 145);
                atualizaPagamento();
                break;
        }
    }

    public void processaTrailer(Cnab240Parser parser) {
    }

    public void processaRetorno(File f) {
        try (Cnab240File reader = new Cnab240File(f)) {
            int registro = -1;
            dataHoraConfirmacao = new Date();
            Cnab240Parser parser = new Cnab240Parser();
            while (registro != Cnab240Parser.TRAILER) {
                parser.setLine(reader.readLine());
                registro = parser.getControle().getRegistro();
                switch (registro) {
                    case Cnab240Parser.HEADER:
                        processaHeader(parser);
                        break;
                    case Cnab240Parser.HEADER_LOTE:
                        processaHeaderLote(parser);
                        break;
                    case Cnab240Parser.SEGMENTO:
                        processaSegmento(parser);
                        break;
                    case Cnab240Parser.TRAILER_LOTE:
                        break;
                    case Cnab240Parser.TRAILER:
                        processaTrailer(parser);
                        break;
                    default:
                        throw new RuntimeException(
                            String.format("Tipo de registro desconhecido: %d", registro));
                }
            }
        }
    }
    
    /**
     * Processa os arquivos contidos no diretório de retorno.
     */
    public void processa() {
            logger.log(Level.INFO, "Processando {0}", retorno.getAbsolutePath());
            processaRetorno(retorno);
            logger.log(Level.INFO, "Processamento finalizado: {0}", retorno.getAbsolutePath());
    }
}
