/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.servlet;

import evento.persistence.controller.ArtigoJpaController;
import evento.persistence.entity.Artigo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Wilson
 */
@WebServlet(name = "ArtigoDownloadServlet", urlPatterns = {"/artigo/download"})
public class ArtigoDownloadServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        File file = null;
        try {
            Artigo a = getArtigo(request);
            if (checkArtigoHash(request, a)) {
                file = getArtigoFile(request, a);
                response.setContentType("application/pdf");
                response.setContentLength((int) file.length());
                response.setHeader("Content-Disposition",
                        String.format("attachment; filename=%s", file.getName()));
                sendFile(file, response.getOutputStream());
            } else {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } catch (Throwable t) {
            log("File: " + (file == null ? null : file.getAbsolutePath()), t);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private Artigo getArtigo(HttpServletRequest request) {
        Artigo artigo = null;
        try {
            int autor = Integer.parseInt(request.getParameter("a"));
            int seq = Integer.parseInt(request.getParameter("s"));
            ArtigoJpaController ctl = new ArtigoJpaController();
            artigo = ctl.findByAutorSeq(autor, seq);
        } catch (Throwable t) {
            throw t;
        }
        return artigo;
    }

    private File getArtigoFile(HttpServletRequest request, Artigo a) {
        String path = request.getServletContext().getRealPath(
                String.format("/WEB-INF/artigos/%d/%s", a.getArtigoPK().getAutor(), a.getArquivo()));
        return new File(path);
    }

    private void sendFile(File file, OutputStream os)
            throws FileNotFoundException, IOException {
        final byte[] buffer = new byte[4096];
        try (FileInputStream fis = new FileInputStream(file);
                FilterOutputStream fos = new FilterOutputStream(os)) {
            int n;
            do {
                n = fis.read(buffer);
                fos.write(buffer, 0, n);
            } while (n == buffer.length);
        }
    }

    public boolean checkArtigoHash(HttpServletRequest request, Artigo a) {
        boolean ok = true;
        if (!request.isUserInRole("admin")) {
            String base = String.format("%011d%s", a.getInscricao().getCpf(),
                    request.getSession(false).getId());
            ok = DigestUtils.sha1Hex(base).equals(request.getParameter("h"));
        }
        return ok;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
