/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Wilson
 */
@Entity
@Table(name = "artigo")
@NamedQueries({
    @NamedQuery(name = "Artigo.findAll", query = "SELECT a FROM Artigo a")})
public class Artigo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ArtigoPK artigoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_submissao", insertable = false, nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataSubmissao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 2147483647)
    @Column(name = "resumo")
    private String resumo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "arquivo")
    private String arquivo;
    @JoinColumn(name = "autor", referencedColumnName = "numero", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Inscricao inscricao;

    @JoinColumn(name = "situacao", referencedColumnName = "codigo", insertable = false, updatable = false, nullable = true)
    @ManyToOne(optional = false)
    private SituacaoArtigo situacao;

    public Artigo() {
    }

    public Artigo(ArtigoPK artigoPK) {
        this.artigoPK = artigoPK;
    }

    public Artigo(ArtigoPK artigoPK, Date dataSubmissao, String titulo, String arquivo) {
        this.artigoPK = artigoPK;
        this.dataSubmissao = dataSubmissao;
        this.titulo = titulo;
        this.arquivo = arquivo;
    }

    public Artigo(int autor, int seq) {
        this.artigoPK = new ArtigoPK(autor, seq);
    }

    public ArtigoPK getArtigoPK() {
        return artigoPK;
    }

    public void setArtigoPK(ArtigoPK artigoPK) {
        this.artigoPK = artigoPK;
    }

    public Date getDataSubmissao() {
        return dataSubmissao;
    }

    public void setDataSubmissao(Date dataSubmissao) {
        this.dataSubmissao = dataSubmissao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public Inscricao getInscricao() {
        return inscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    public SituacaoArtigo getSituacao() {
        return situacao;
    }

    public void setSituacao(SituacaoArtigo situacao) {
        this.situacao = situacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (artigoPK != null ? artigoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Artigo)) {
            return false;
        }
        Artigo other = (Artigo) object;
        if ((this.artigoPK == null && other.artigoPK != null) || (this.artigoPK != null && !this.artigoPK.equals(other.artigoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "evento.persistence.entity.Artigo[ artigoPK=" + artigoPK + " ]";
    }
    
}
