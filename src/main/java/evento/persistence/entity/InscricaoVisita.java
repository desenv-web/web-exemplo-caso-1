/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Wilson
 */
@Entity
@Table(name = "inscricao_visita")
@NamedQueries({
    @NamedQuery(name = "InscricaoVisita.findAll", query = "SELECT i FROM InscricaoVisita i")})
public class InscricaoVisita implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InscricaoVisitaPK inscricaoVisitaPK;
    @Basic(optional = false)
    @Column(name = "data_hora", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHora;
    @JoinColumn(name = "numero_inscricao", referencedColumnName = "numero", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Inscricao inscricao;
    @JoinColumn(name = "situacao", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Situacao situacao;
    @JoinColumn(name = "visita", referencedColumnName = "codigo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Visita visita;

    public InscricaoVisita() {
    }

    public InscricaoVisita(InscricaoVisitaPK inscricaoVisitaPK) {
        this.inscricaoVisitaPK = inscricaoVisitaPK;
    }

    public InscricaoVisita(InscricaoVisitaPK inscricaoVisitaPK, Date dataHora) {
        this.inscricaoVisitaPK = inscricaoVisitaPK;
        this.dataHora = dataHora;
    }

    public InscricaoVisita(int numeroInscricao, int visita) {
        this.inscricaoVisitaPK = new InscricaoVisitaPK(numeroInscricao, visita);
    }

    public InscricaoVisitaPK getInscricaoVisitaPK() {
        return inscricaoVisitaPK;
    }

    public void setInscricaoVisitaPK(InscricaoVisitaPK inscricaoVisitaPK) {
        this.inscricaoVisitaPK = inscricaoVisitaPK;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public Inscricao getInscricao() {
        return inscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Visita getVisita() {
        return visita;
    }

    public void setVisita(Visita visita) {
        this.visita = visita;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inscricaoVisitaPK != null ? inscricaoVisitaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InscricaoVisita)) {
            return false;
        }
        InscricaoVisita other = (InscricaoVisita) object;
        if ((this.inscricaoVisitaPK == null && other.inscricaoVisitaPK != null) || (this.inscricaoVisitaPK != null && !this.inscricaoVisitaPK.equals(other.inscricaoVisitaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "evento.persistence.entity.InscricaoVisita[ inscricaoVisitaPK=" + inscricaoVisitaPK + " ]";
    }
    
}
