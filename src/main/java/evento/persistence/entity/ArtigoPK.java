/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Wilson
 */
@Embeddable
public class ArtigoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "autor")
    private int autor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seq")
    private int seq;

    public ArtigoPK() {
    }

    public ArtigoPK(int autor, int seq) {
        this.autor = autor;
        this.seq = seq;
    }

    public int getAutor() {
        return autor;
    }

    public void setAutor(int autor) {
        this.autor = autor;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) autor;
        hash += (int) seq;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArtigoPK)) {
            return false;
        }
        ArtigoPK other = (ArtigoPK) object;
        if (this.autor != other.autor) {
            return false;
        }
        if (this.seq != other.seq) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "evento.persistence.entity.ArtigoPK[ autor=" + autor + ", seq=" + seq + " ]";
    }
    
}
