/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Wilson
 */
@Entity
@Table(name = "inscricao_minicurso")
@NamedQueries({
    @NamedQuery(name = "InscricaoMinicurso.findAll", query = "SELECT i FROM InscricaoMinicurso i")})
public class InscricaoMinicurso implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InscricaoMinicursoPK inscricaoMinicursoPK;
    @Basic(optional = false)
    @Column(name = "data_hora", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHora;
    @JoinColumn(name = "numero_inscricao", referencedColumnName = "numero", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Inscricao inscricao;
    @JoinColumn(name = "minicurso", referencedColumnName = "codigo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Minicurso minicurso;
    @JoinColumn(name = "situacao", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Situacao situacao;

    public InscricaoMinicurso() {
    }

    public InscricaoMinicurso(InscricaoMinicursoPK inscricaoMinicursoPK) {
        this.inscricaoMinicursoPK = inscricaoMinicursoPK;
    }

    public InscricaoMinicurso(InscricaoMinicursoPK inscricaoMinicursoPK, Date dataHora) {
        this.inscricaoMinicursoPK = inscricaoMinicursoPK;
        this.dataHora = dataHora;
    }

    public InscricaoMinicurso(int numeroInscricao, int minicurso) {
        this.inscricaoMinicursoPK = new InscricaoMinicursoPK(numeroInscricao, minicurso);
    }

    public InscricaoMinicursoPK getInscricaoMinicursoPK() {
        return inscricaoMinicursoPK;
    }

    public void setInscricaoMinicursoPK(InscricaoMinicursoPK inscricaoMinicursoPK) {
        this.inscricaoMinicursoPK = inscricaoMinicursoPK;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public Inscricao getInscricao() {
        return inscricao;
    }

    public void setInscricao(Inscricao inscricao) {
        this.inscricao = inscricao;
    }

    public Minicurso getMinicurso() {
        return minicurso;
    }

    public void setMinicurso(Minicurso minicurso) {
        this.minicurso = minicurso;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inscricaoMinicursoPK != null ? inscricaoMinicursoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InscricaoMinicurso)) {
            return false;
        }
        InscricaoMinicurso other = (InscricaoMinicurso) object;
        if ((this.inscricaoMinicursoPK == null && other.inscricaoMinicursoPK != null) || (this.inscricaoMinicursoPK != null && !this.inscricaoMinicursoPK.equals(other.inscricaoMinicursoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "evento.persistence.entity.InscricaoMinicurso[ inscricaoMinicursoPK=" + inscricaoMinicursoPK + " ]";
    }
    
}
