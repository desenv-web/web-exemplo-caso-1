/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.controller;

import evento.persistence.entity.Inscricao;
import evento.persistence.entity.InscricaoMinicurso;
import evento.persistence.entity.InscricaoMinicursoPK;
import evento.persistence.entity.InscricaoVisita;
import evento.persistence.entity.InscricaoVisitaPK;
import evento.persistence.entity.Inscricao_;
import evento.persistence.entity.Situacao;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Wilson
 */
public class InscricaoJpaController extends JpaController {

    // código da situação de inscrição cancelada
    public static final Situacao SITUACAO_PENDENTE   = new Situacao(1);
    public static final Situacao SITUACAO_CONFIRMADO = new Situacao(2);
    public static final Situacao SITUACAO_CANCELADO  = new Situacao(3);

    public InscricaoJpaController() {
    }

    public InscricaoJpaController(boolean lazy) {
        super(lazy);
    }

    public List<Inscricao> findOrderByNumero(int first, int max) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.orderBy(cb.asc(rt.get(Inscricao_.numero)));
            TypedQuery<Inscricao> q = em.createQuery(cq);
            q.setFirstResult(first);
            q.setMaxResults(max);
            return q.getResultList();
        } catch (NoResultException e) {
            return Collections.emptyList();
        } finally {
            closeNotLazy();
        }
    }

    public List<Inscricao> findAllOrderByNumero() {
        return findOrderByNumero(0, Integer.MAX_VALUE);
    }

    public Inscricao findByNumero(Integer numero) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inscricao.class, numero);
        } catch (NoResultException e) {
            return null;
        } finally {
            closeNotLazy();
        }
    }
    
    /**
     * Localiza um inscrição pelo CPF. Caso não exista, retorna {@code null}.
     * @param cpf O CPF da inscrição.
     * @return A inscrição ou {@code null} caso não exista.
     */
    public Inscricao findByCPF(Long cpf) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.where(cb.equal(rt.get(Inscricao_.cpf), cpf));
            TypedQuery<Inscricao> q = em.createQuery(cq);
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } finally {
            closeNotLazy();
        }
    }

    public List<Inscricao> findBySituacao(int situacao) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.where(cb.equal(rt.get(Inscricao_.situacao), situacao));
            TypedQuery<Inscricao> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }
    
    public List<Inscricao> findByArtigos() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.where(cb.isNotEmpty(rt.get(Inscricao_.artigoList))).orderBy(cb.asc(rt.get(Inscricao_.numero)));
            TypedQuery<Inscricao> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }
    
    private void inicializaInscricaoMinicursoList(Inscricao inscricao, List<InscricaoMinicurso> imcList) {
        if (imcList == null) return;
        for (InscricaoMinicurso imc: imcList) {
            imc.setInscricao(inscricao);
            imc.setInscricaoMinicursoPK(new InscricaoMinicursoPK(inscricao.getNumero(), imc.getMinicurso().getCodigo()));
        }
    }
    
    private void inicializaInscricaoVisitaList(Inscricao inscricao, List<InscricaoVisita> ivList) {
        if (ivList == null) return;
        for (InscricaoVisita iv: ivList) {
            iv.setInscricao(inscricao);
            iv.setInscricaoVisitaPK(new InscricaoVisitaPK(inscricao.getNumero(), iv.getVisita().getCodigo()));
        }
    }
    
    public Inscricao persist(Inscricao inscricao) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            
            List<InscricaoMinicurso> imcList = inscricao.getInscricaoMinicursoList();
            inscricao.setInscricaoMinicursoList(null);
            List<InscricaoVisita> ivList = inscricao.getInscricaoVisitaList();
            inscricao.setInscricaoVisitaList(null);
            Inscricao insc = em.merge(inscricao);
            
            inicializaInscricaoMinicursoList(insc, imcList);
            insc.setInscricaoMinicursoList(imcList);
            
            inicializaInscricaoVisitaList(insc, ivList);
            insc.setInscricaoVisitaList(ivList);
            em.persist(insc);
            em.getTransaction().commit();
            return insc;
        } finally {
            closeNotLazy();
        }
    }
    
    public void remove(Inscricao inscricao) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            Inscricao i = em.find(Inscricao.class, inscricao.getNumero());
            em.remove(i);
            em.getTransaction().commit();
        } finally {
            closeNotLazy();
        }
    }
    
    public int countInscritos() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<Inscricao> rt = cq.from(Inscricao.class);
            cq.select(cb.count(rt));
            // não conta os cancelados
            cq.where(cb.notEqual(rt.get(Inscricao_.situacao),
                        InscricaoJpaController.SITUACAO_CANCELADO));
            TypedQuery<Long> q = em.createQuery(cq);
            return q.getSingleResult().intValue();
        } finally {
            closeNotLazy();
        }
    }

}
