/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.controller;

import evento.persistence.entity.InscricaoVisita;
import evento.persistence.entity.InscricaoVisitaPK_;
import evento.persistence.entity.InscricaoVisita_;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Wilson
 */
public class InscricaoVisitaJpaControler extends JpaController {

    public InscricaoVisitaJpaControler() {
    }

    public InscricaoVisitaJpaControler(boolean lazy) {
        super(lazy);
    }

    /**
     * Retorna o número de inscritos em uma determinada visita excluindo
     * as inscrições canceladas.
     * @param visita O código da visita
     * @return O número de inscritos
     */
    public int countInscritos(int visita) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<InscricaoVisita> rt = cq.from(InscricaoVisita.class);
            cq.select(cb.count(rt));
            // não conta os cancelados
            cq.where(cb.notEqual(rt.get(InscricaoVisita_.situacao),
                        InscricaoJpaController.SITUACAO_CANCELADO),
                    cb.equal(rt.get(InscricaoVisita_.inscricaoVisitaPK).
                        get(InscricaoVisitaPK_.visita), visita));
            TypedQuery<Long> q = em.createQuery(cq);
            return q.getSingleResult().intValue();
        } finally {
            closeNotLazy();
        }
    }

}
