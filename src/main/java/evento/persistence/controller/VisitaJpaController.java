package evento.persistence.controller;

import evento.persistence.entity.Visita;
import evento.persistence.entity.Visita_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class VisitaJpaController extends JpaController {

    public VisitaJpaController() {
    }

    public VisitaJpaController(boolean lazy) {
        super(lazy);
    }

    public List<Visita> findAll() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Visita> cq = cb.createQuery(Visita.class);
            Root<Visita> rt = cq.from(Visita.class);
            cq.orderBy(cb.asc(rt.get(Visita_.codigo)));
            TypedQuery<Visita> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }

}
