package evento.persistence.controller;

import evento.persistence.entity.Artigo;
import evento.persistence.entity.ArtigoPK;
import evento.persistence.entity.Artigo_;
import evento.persistence.entity.Inscricao_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

/**
 *
 * @author Wilson
 */
public class ArtigoJpaController extends JpaController {

    public ArtigoJpaController() {
    }

    public Artigo findByPk(ArtigoPK pk) {
        Artigo a;
        EntityManager em = getEntityManager();
        try {
            a = em.find(Artigo.class, pk);
        } finally {
            closeNotLazy();
        }
        return a;
    }
    
    public Artigo findByAutorSeq(int autor, int seq) {
        return findByPk(new ArtigoPK(autor, seq));
    }
    
    public List<Artigo> findAllOrderBy(Order... order) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Artigo> cq = cb.createQuery(Artigo.class);
            Root<Artigo> rt = cq.from(Artigo.class);
            cq.orderBy(order);
            TypedQuery<Artigo> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }
    
    public List<Artigo> findAllOrderByInscricao() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Artigo> cq = cb.createQuery(Artigo.class);
            Root<Artigo> rt = cq.from(Artigo.class);
            return findAllOrderBy(cb.asc(rt.get(Artigo_.inscricao).get(Inscricao_.numero)));
        } finally {
            closeNotLazy();
        }
    }
    
    public void persist(Artigo artigo) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(artigo);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }
}
