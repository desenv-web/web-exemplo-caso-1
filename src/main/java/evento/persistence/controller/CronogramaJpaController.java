package evento.persistence.controller;

import evento.persistence.entity.Cronograma;

/**
 *
 * @author Wilson
 */
public class CronogramaJpaController extends JpaController {

    private Cronograma cronograma;
    
    public CronogramaJpaController() {
    }
    
    public Cronograma getCronograma() {
        try {
            if (cronograma == null) {
                cronograma = getEntityManager().find(Cronograma.class, 1);
            }
        } finally {
            close();
        }
        return cronograma;
    }
    
}
