/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.controller;

import evento.persistence.entity.Minicurso;
import evento.persistence.entity.Minicurso_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Wilson
 */
public class MinicursoJpaController extends JpaController {

    public MinicursoJpaController() {
    }

    public MinicursoJpaController(boolean lazy) {
        super(lazy);
    }

    public List<Minicurso> findAll() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Minicurso> cq = cb.createQuery(Minicurso.class);
            Root<Minicurso> rt = cq.from(Minicurso.class);
            cq.orderBy(cb.asc(rt.get(Minicurso_.codigo)));
            TypedQuery<Minicurso> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }
}
