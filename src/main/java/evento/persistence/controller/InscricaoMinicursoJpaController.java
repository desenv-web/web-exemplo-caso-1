/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.controller;

import evento.persistence.entity.InscricaoMinicurso;
import evento.persistence.entity.InscricaoMinicursoPK_;
import evento.persistence.entity.InscricaoMinicurso_;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Wilson
 */
public class InscricaoMinicursoJpaController extends JpaController{

    public InscricaoMinicursoJpaController() {
    }

    public InscricaoMinicursoJpaController(boolean lazy) {
        super(lazy);
    }

    /**
     * Retorna o número de inscritos em um determinado minicurso excluindo
     * as inscrições canceladas.
     * @param minicurso O código do minicurso
     * @return O número de inscritos
     */
    public int countInscritos(int minicurso) {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<InscricaoMinicurso> rt = cq.from(InscricaoMinicurso.class);
            cq.select(cb.count(rt));
            // não conta os cancelados
            cq.where(cb.notEqual(rt.get(InscricaoMinicurso_.situacao),
                        InscricaoJpaController.SITUACAO_CANCELADO),
                    cb.equal(rt.get(InscricaoMinicurso_.inscricaoMinicursoPK).
                        get(InscricaoMinicursoPK_.minicurso), minicurso));
            TypedQuery<Long> q = em.createQuery(cq);
            return q.getSingleResult().intValue();
        } finally {
            closeNotLazy();
        }
    }
}
