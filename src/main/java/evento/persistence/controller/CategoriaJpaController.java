/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package evento.persistence.controller;

import evento.persistence.entity.Categoria;
import evento.persistence.entity.Categoria_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Wilson
 */
public class CategoriaJpaController extends JpaController {

    public CategoriaJpaController() {
    }

    public CategoriaJpaController(boolean lazy) {
        super(lazy);
    }

    public List<Categoria> findAll() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Categoria> cq = cb.createQuery(Categoria.class);
            Root<Categoria> rt = cq.from(Categoria.class);
            cq.orderBy(cb.asc(rt.get(Categoria_.codigo)));
            TypedQuery<Categoria> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }
}
