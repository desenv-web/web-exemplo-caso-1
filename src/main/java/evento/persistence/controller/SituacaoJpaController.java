package evento.persistence.controller;

import evento.persistence.entity.Situacao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Wilson
 */
public class SituacaoJpaController extends JpaController {

    public SituacaoJpaController() {
    }
    
    public Situacao findByCodigo(int codigo) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Situacao.class, codigo);
        } finally {
            closeNotLazy();
        }
    }
    
    public List<Situacao> findAll() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Situacao> cq = cb.createQuery(Situacao.class);
            cq.from(Situacao.class);
            TypedQuery<Situacao> q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            closeNotLazy();
        }
    }
    
}
