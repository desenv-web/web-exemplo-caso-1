/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Wilson
 */
public class FixedColumnParser {
    private String line;
    private int currPos = 1;

    public FixedColumnParser() {
    }

    public FixedColumnParser(String line) {
        this.currPos = 0;
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    /**
     * Returns a field given the first and last position. Line starts at position 1.
     * @param start The start position
     * @param stop The last position (inclusive)
     * @return Field value as a string.
     */
    public String getField(int start, int stop) {
        return line.substring(start-1, stop);
    }

    public Integer getIntField(int start, int stop) {
        return Integer.valueOf(getField(start, stop));
    }

    public Long getLongField(int start, int stop) {
        return Long.valueOf(getField(start, stop));
    }

    public Date parseDate(String value) {
        if (value.length() != 8) {
            throw new RuntimeException(String.format("Invalid date field: %s", value));
        }
        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        try {
            return sdf.parse(value);
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    public Date getDateField(int start, int stop) {
        return parseDate(getField(start, stop));
    }

    public String getNext(int len) {
        String value = getField(currPos, len);
        currPos += len;
        return value;
    }

    public Long getNextLong(int len) {
        return Long.valueOf(getNext(len));
    }

    public Integer getNextInt(int len) {
        return Integer.valueOf(getNext(len));
    }

    public Date getNextDate(int len) {
        return parseDate(getNext(len));
    }
}
