/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

/**
 *
 * @author Wilson
 */
public class TextFile implements AutoCloseable {

    private File file;
    private LineNumberReader reader;

    public TextFile() {
    }

    public TextFile(File file) {
        this.file = file;
    } 
    
    public TextFile(String filePath) {
        this(new File(filePath));
    }

    public LineNumberReader getReader() {
        if (reader == null) {
            try {
                reader = new LineNumberReader(new FileReader(file));
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        }
        return reader;
    }

    @Override
    public void close() {
        try {
            getReader().close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String readLine() {
        try {
            return getReader().readLine();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
