/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text.cnab;

/**
 *
 * @author Wilson
 */
public class Cnab240Controle {
    private int banco;
    private int lote;
    private int registro;

    public Cnab240Controle() {
    }

    public int getBanco() {
        return banco;
    }

    public void setBanco(int banco) {
        this.banco = banco;
    }

    public int getLote() {
        return lote;
    }

    public void setLote(int lote) {
        this.lote = lote;
    }

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }

}
