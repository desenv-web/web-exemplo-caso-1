/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text.cnab;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 *
 * @author Wilson
 */
public class Cnab240Inscricao {
    @Min(0)
    @Max(9)
    private int tipo;
    @Min(0)
    @Max(99999999999999L)
    private long numero;
}
