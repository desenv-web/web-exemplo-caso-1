/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text.cnab;

import utfpr.file.text.parser.FixedColumnParser;

/**
 *
 * @author Wilson
 */
public class Cnab240Parser extends FixedColumnParser {

    public static final int HEADER          = 0;
    public static final int HEADER_LOTE     = 1;
    public static final int SEGMENTO        = 3;
    public static final int TRAILER_LOTE    = 5;
    public static final int TRAILER         = 9;

    public static final String SEGMENTO_T = "T";
    public static final String SEGMENTO_U = "U";

    public Cnab240Parser() {
        super();
    }

    public Cnab240Parser(String line) {
        super(line);
    }

    public Cnab240Controle getControle() {
        Cnab240Controle controle = new Cnab240Controle();
        controle.setBanco(getIntField(1, 3));
        controle.setLote(getIntField(4, 7));
        controle.setRegistro(getIntField(8, 8));
        return controle;
    }

    public int getCodigoMovimento() {
        return getIntField(16, 17);
    }
}
