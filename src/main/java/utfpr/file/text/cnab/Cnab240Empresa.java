/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text.cnab;

import javax.validation.constraints.Size;

/**
 *
 * @author Wilson
 */
public class Cnab240Empresa {
    private Cnab240Inscricao inscricao;
    private String convenio;
    private Cnab240ContaCorrente contaCorrente;
    @Size(max = 30)
    private String nome;
}
