/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.file.text.cnab;

import java.io.File;
import utfpr.file.text.TextFile;

/**
 *
 * @author Wilson
 */
public class Cnab240File extends TextFile {

    public static final int LINE_LEN = 240;

    public Cnab240File(File file) {
        super(file);
    }

    public Cnab240File(String filePath) {
        super(filePath);
    }
    
    @Override
    public String readLine() {
        String line = super.readLine();
        if (line.length() != LINE_LEN) {
            throw new RuntimeException("Formato de linha incorreto");
        }
        return line;
    }

}
