/*
 * === CLASSES DE APOIO JSF ===
 * Este código fonte pode ser livremente copiado, modificado e distribuído,
 * desde que esta mensagem seja preservada.
 * Todo o esforço foi feito para escrever um código correto e eficiente, entretanto o
 * autor não assume qualquer responsabilidade, direta ou indireta sobre a aplicabilidade
 * e/ou adequação a um fim específico e não garante que esteja livre de erros.
 * Copyright (C) 2009 <a href="http://www.utfpr.edu.br">Universidade Tecnológica Federal do Paraná</a>
 */

package utfpr.faces.support;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * Classe base para os beans de aplicação, sessão, request e página.
 * @version 1.0.0
 *
 * @author Wilson Horstmeyer Bogado <wilson@dainf.ct.utfpr.edu.br>
 * <a href="http://www.utfpr.edu.br">Universidade Tecnológica Federal do Paraná</a>
 */
public class FacesBean implements Serializable {

    private static final Logger logger = Logger.getLogger(PageBean.class.getName());

    /**
     * Retorna o <code>FacesContext</code> corrente.
     * @return O <code>FacesContext</code>
     */
    public FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /**
     * Retorna o contexto externo de JSF.
     * @return O contexto externo
     */
    public ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    /**
     * Gera um log de informação com a mensagem especificada.
     * @param message A mensagem
     */
    public void log(String message) {
        logger.log(Level.INFO, message);
    }

    /**
     * Gera um log de erro com a mensagem e exceção especificadas.
     * @param message A mensagem
     * @param t A exceção
     */
    public void log(String message, Throwable t) {
        logger.log(Level.SEVERE, message, t);
    }
}
